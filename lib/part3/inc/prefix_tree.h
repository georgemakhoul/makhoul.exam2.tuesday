#ifndef CMPE126EXAM1_TUESDAY_PREFIX_TREE_H
#define CMPE126EXAM1_TUESDAY_PREFIX_TREE_H

#include "prefix_node.h"
#include <string>
#include <vector>

namespace part3{
    class prefix_tree{
        prefix_node* root;
        prefix_node* current_node;

        int size;

    public:
        explicit prefix_tree(char root_value);
        ~prefix_tree();

        void insert(char to_insert);

        std::vector<std::string> starts_with(std::string prefix);
        std::vector<std::string> exactly_n_digits(int n);
        std::vector<std::string> longer_than_n_digits(int n);
        std::vector<std::string> shorter_than_n_digits(int n);

        int longest();

        int get_size();
    };
}

#endif
