#include "prefix_tree.h"

part3::prefix_tree::prefix_tree(char key) {
    root = new prefix_node(key);
    size = 1;
}

part3::prefix_tree::~prefix_tree() {

}

void part3::prefix_tree::insert(char to_insert) {

}

std::vector<std::string> part3::prefix_tree::starts_with(std::string prefix) {

    return std::vector<std::string>();
}

std::vector<std::string> part3::prefix_tree::exactly_n_digits(int n) {
    return std::vector<std::string>();
}

std::vector<std::string> part3::prefix_tree::longer_than_n_digits(int n) {
    return std::vector<std::string>();
}

std::vector<std::string> part3::prefix_tree::shorter_than_n_digits(int n) {
    return std::vector<std::string>();
}

int part3::prefix_tree::longest() {
    return 0;
}

int part3::prefix_tree::get_size() {
    return size;
}

