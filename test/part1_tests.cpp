#include "gtest/gtest.h"

int number_of_2s(int input){
    int count = 0;
    while(input > 0){
        if(input % 10 == 2) count++;
        input = input / 10;
    }
    return count;
}

int number_of_2s_in_range(int start, int end){
    int count = 0;
    while(start <= end){
        count += number_of_2s(start++);
    }
    return count;
}

TEST(Part1, Test1){
    EXPECT_EQ(1, number_of_2s(250));
    EXPECT_NE(2, number_of_2s(250));
    EXPECT_EQ(0, number_of_2s(-56833947));
    EXPECT_NE(1, number_of_2s(-1));
    EXPECT_NO_THROW(number_of_2s(1));
    EXPECT_FALSE(false);
}

TEST(Part1, Test2){
    EXPECT_EQ(0, number_of_2s_in_range(15,9));
    EXPECT_EQ(1, number_of_2s_in_range(-24,5));
    EXPECT_NE(0, number_of_2s_in_range(42,96));
    EXPECT_FALSE(false);
}

TEST(Part1, Test3){
    EXPECT_EQ(1, number_of_2s_in_range(1,2));
    EXPECT_NE(5, number_of_2s_in_range(0,2));
    EXPECT_TRUE(true);
}