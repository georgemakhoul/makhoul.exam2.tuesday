#include "gtest/gtest.h"
#include "../lib/part3/inc/prefix_tree.h"

TEST(PrefixNodeTests, constructor){
    part3::prefix_node test_node = part3::prefix_node('3');
}

TEST(PrefixNodeTests, assignment){
    auto *test_node_1 = new part3::prefix_node('1');
    auto *test_node_2 = new part3::prefix_node('2');

    char insert_location = test_node_2->value;

    test_node_1->children[insert_location] = test_node_2;
    EXPECT_EQ(test_node_2, test_node_1->children[test_node_2->value]);
    EXPECT_EQ('2', test_node_1->children['2']->value);
}

TEST(Part3, DISABLED_simple_test){
    part3::prefix_tree* prefix_tree_UT;

    prefix_tree_UT = new part3::prefix_tree('1');
    prefix_tree_UT->insert('2');
    prefix_tree_UT->insert('2');
    prefix_tree_UT->insert('2');

    EXPECT_EQ(1222, prefix_tree_UT->longest());

    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('4');
    prefix_tree_UT->insert('2');
    EXPECT_EQ(1222, prefix_tree_UT->longest());


    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('1');

    EXPECT_EQ(1222, prefix_tree_UT->longest());

    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('4');
    prefix_tree_UT->insert('2');
    prefix_tree_UT->insert('4');
    prefix_tree_UT->insert('2');
    EXPECT_EQ(14242, prefix_tree_UT->longest());

    EXPECT_EQ(15, prefix_tree_UT->get_size());

    delete prefix_tree_UT;
}

TEST(Part3, DISABLED_starts_with_tests){
    part3::prefix_tree* prefix_tree_UT;

    prefix_tree_UT = new part3::prefix_tree('0');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('2');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('3');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('4');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('5');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('6');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('7');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('8');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('9');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('1');

    std::vector<std::string> starts_with_values = prefix_tree_UT->starts_with("01");

    EXPECT_EQ(2, starts_with_values.size());
    std::string value_0 = starts_with_values[0];
    EXPECT_TRUE(value_0 == std::string("01") || value_0 == std::string("011"));

    std::string value_1 = starts_with_values[1];
    EXPECT_TRUE((value_1 == std::string("01") || value_1 == std::string("011")) && (value_1 != value_0));
    delete prefix_tree_UT;
}

TEST(Part3, DISABLED_exactly_n_digits){
    part3::prefix_tree* prefix_tree_UT;

    prefix_tree_UT = new part3::prefix_tree('0');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('2');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('3');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('4');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('5');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('6');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('7');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('8');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('9');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('1');

    std::vector<std::string> exactly_3_digits = prefix_tree_UT->exactly_n_digits(3);

    EXPECT_EQ(1, exactly_3_digits.size());
    EXPECT_EQ(std::string("011"), exactly_3_digits[0]);

    prefix_tree_UT->insert('3');

    exactly_3_digits = prefix_tree_UT->exactly_n_digits(3);

    EXPECT_EQ(0, exactly_3_digits.size());

    std::vector<std::string> exactly_2_digits = prefix_tree_UT->exactly_n_digits(2);
    EXPECT_EQ(9, exactly_2_digits.size());
    delete prefix_tree_UT;
}

TEST(Part3, DISABLED_shorter_than_n_digits){
    part3::prefix_tree* prefix_tree_UT;

    prefix_tree_UT = new part3::prefix_tree('0');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('2');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('3');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('4');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('5');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('6');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('7');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('8');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('9');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('1');

    std::vector<std::string> shorter_than_2_digits = prefix_tree_UT->shorter_than_n_digits(2);

    EXPECT_EQ(0, shorter_than_2_digits.size());
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('0');

    shorter_than_2_digits = prefix_tree_UT->shorter_than_n_digits(2);
    EXPECT_EQ(1, shorter_than_2_digits.size());
    EXPECT_EQ(std::string("0"), shorter_than_2_digits[0]);

    std::vector<std::string> shorter_than_3_digits = prefix_tree_UT->shorter_than_n_digits(3);

    EXPECT_EQ(10, shorter_than_2_digits.size());

    delete prefix_tree_UT;
}

TEST(Part3, DISABLED_longer_than_n_digits){
    part3::prefix_tree* prefix_tree_UT;

    prefix_tree_UT = new part3::prefix_tree('0');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('2');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('3');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('4');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('5');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('6');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('7');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('8');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('9');
    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('1');

    std::vector<std::string> longer_than_2_digits = prefix_tree_UT->longer_than_n_digits(2);

    EXPECT_EQ(1, longer_than_2_digits.size());
    EXPECT_EQ(std::string("011"), longer_than_2_digits[0]);
    prefix_tree_UT->insert('4');
    prefix_tree_UT->insert('2');

    longer_than_2_digits = prefix_tree_UT->longer_than_n_digits(2);

    EXPECT_EQ(1, longer_than_2_digits.size());
    EXPECT_EQ(std::string("01142"), longer_than_2_digits[0]);

    prefix_tree_UT->insert('0');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('1');
    prefix_tree_UT->insert('3');

    longer_than_2_digits = prefix_tree_UT->longer_than_n_digits(2);

    EXPECT_EQ(2, longer_than_2_digits.size());
    std::string value_1 = longer_than_2_digits[0];
    std::string value_2 = longer_than_2_digits[1];

    std::string expected_a = "0113";
    std::string expected_b = "01142";

    EXPECT_FALSE(value_1 == value_2);
    EXPECT_TRUE((value_1 == expected_a) || (value_1 == expected_b));
    EXPECT_TRUE((value_2 == expected_a) || (value_2 == expected_b));

    delete prefix_tree_UT;
}